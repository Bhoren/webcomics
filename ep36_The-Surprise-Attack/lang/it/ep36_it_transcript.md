# Transcript of Pepper&Carrot Episode 36 [it]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|Episodio 36: Attacco a sorpresa

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Wasabi|1|True|DELLE SCUSE?
Wasabi|2|False|Ma stai scherzando!
Wasabi|3|False|RINCHIUDETE QUESTA SCIOCCA IN PRIGIONE!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|False|Fs ch h…
Pepper|2|True|Accidenti!
Pepper|3|False|Qui dentro non posso fare nulla!
Pepper|4|False|Maledetta prigione magica! Grrrr!
Sound|5|False|SDANG !

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Come posso essere stata così ingenua!
Shichimi|2|True|Shhhhh, Pepper!
Shichimi|3|False|Fai meno rumore.
Pepper|4|False|Chi è là?!
Shichimi|5|True|Shhhhh! Zitta!
Shichimi|6|True|Avvicinati.
Shichimi|7|False|Sono qui per liberarti.
Sound|8|False|Kshiii…
Pepper|9|False|Shichimi?

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|True|Mi… Mi dispiace per quello che è successo…
Shichimi|2|True|…lo sai, il nostro litigio.
Shichimi|3|True|Io…
Shichimi|4|False|Io ho dovuto.
Pepper|5|True|Non ti preoccupare – lo so.
Pepper|6|False|Grazie per essere qui.
Shichimi|7|False|Questa cella magica è davvero potente – l'hanno creata apposta per te!
Pepper|8|False|Ha ha!
Shichimi|9|False|Fai piano, o ci sentiranno.
Rat|10|True|SLAP
Rat|11|True|SLAP
Rat|12|False|SLAP
Shichimi|13|True|Lo sai,
Shichimi|14|True|sono qui anche perché dopo la cerimonia sono stata ammessa nel cerchio magico di Wasabi.
Shichimi|15|False|Ed ho saputo dei suoi piani…

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|6|True|SDENG !
Shichimi|7|True|SDANG !
Shichimi|8|False|SDENG !
Pepper|9|True|SDONK !
Pepper|10|True|SDENG !
Pepper|11|False|È terribile Pepper.
Shichimi|12|True|Wasabi vuole dominare, in modo puro e semplice, tutte le altre scuole di magia…
Shichimi|13|True|Partirà domani all'alba con un esercito di streghe per Qualicity…
Shichimi|14|False|Oh, no!
Shichimi|15|False|Coriandolo!
Sound|16|False|Ed il suo regno!
Pepper|17|True|E la magia di Zombiah!
Pepper|18|False|Dobbiamo avvisarla subito!
Rat|19|False|Un pilota con il suo drago ci aspetta fuori, per portarci via da qui.
Carrot|20|False|Ecco, finalmente la serratura si è aperta!
Rat|21|False|Clang !
Sound|1|True|Brava!
Sound|2|True|Prendo Carrot ed il mio cappello.
Sound|3|True|squeak!
Sound|4|True|HI SS SS !
Sound|5|False|squiiiiiit!
Pepper|22|False|! ! !
Shichimi|23|False|! ! !
Guard|24|True|GUARDIE!
Guard|25|True|VENITE QUI!
Guard|26|False|UN INTRUSO STA LIBERADO IL PRIGIONIERO!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|False|Eravamo così vicine…
Pepper|2|False|Si, ad un soffio.
Pepper|3|False|In ogni caso, sai perché Wasabi ce l'ha con me?
Shichimi|4|True|Lei è spaventata dalle streghe di Chaosah, Pepper…
Shichimi|5|True|In modo particolare per le vostre reazioni a catena.
Shichimi|6|False|Lei crede che siano una grave minaccia per i suoi piani.
Pepper|7|True|Oh, quello, pfuff…
Pepper|8|False|Non c'è bisogno che si preoccupi; non sono mai riuscita a farne partire una.
Shichimi|9|False|Veramente?
Pepper|10|False|Si, davvero, ha ha ha!
Shichimi|11|True|Hee-hee!
Shichimi|12|False|Lei è così paranoica…

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
King|1|True|Ufficiale!
King|2|False|Potete confermarmi che questo è il tempio di quella strega?
Officer|3|True|Altamente probabile, mio signore!
Officer|4|False|Molte delle nostre spie l'hanno avvistata qui di recente.
King|5|True|GRrrr…
King|6|False|Quindi è lì che vive questa minaccia che danneggia la nostra arte della guerra e le nostre tradizioni!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
King|1|False|Celebriamo la nostra alleanza distruggendo per rappresaglia questo tempio dalla superficie di Hereva.
Sound|2|False|Clap
Enemy|3|False|Ben detto!
Army|4|True|Yeah!
Army|5|True|Yeah!
Army|6|True|Yeah!
Army|7|True|Yeah!
Army|8|True|Yeehaaa!
Army|9|True|Yarr!
Army|10|True|Yeehaw !
Army|11|True|Yeee-haw!
Army|12|True|Yaaa!
Army|13|False|Hurrah!
King|14|True|CATAPULTE!
King|15|False|FUOCO!!!
Sound|16|False|huHUOOOOOOOOOOO !
Sound|17|True|Swhooosh!
Sound|18|False|Swhooosh!
King|19|False|ALL'ATTACCOOOO!!!
Pepper|20|True|Che succede?
Pepper|21|False|Un attacco?!
Sound|22|True|B OO M!
Sound|23|False|B O OO M!
Shichimi|24|False|Cosa?!
Sound|25|True|BO O ~ O O O M !
Sound|26|False|B O O M !

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|cough
Pepper|2|False|cough!!
Pepper|3|False|Shichimi! Stai bene?
Shichimi|4|True|Si, niente di rotto!
Shichimi|5|True|E tu come stai?
Shichimi|6|False|E Carrot?
Pepper|7|False|Siamo a posto.
Pepper|8|True|Cosa diavolo è successo…
Pepper|9|False|Io… non… ci posso credere…
Shichimi|10|True|Da dove saltano fuori quei soldati?!
Shichimi|11|True|E con le catapulte?!
Shichimi|12|False|Che cosa vorranno?
Pepper|13|True|Non saprei…
Pepper|14|False|Ma io conosco quei due .
Shichimi|15|False|?
Shichimi|16|False|! ! !
Shichimi|17|False|Torreya, da questa parte!

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Torreya|1|True|Shichimi!
Torreya|2|False|Che gli spiriti siano lodati, tu sei sana e salva.
Sound|3|False|Clap
Torreya|4|True|Ero così preoccupata quando ho sentito che ti avevano imprigionata!
Torreya|5|True|E questa battaglia!
Torreya|6|False|Che caos!
Shichimi|7|False|Torreya, è così bello rivederti.
Pepper|8|True|Oh, cavolo…
Pepper|9|True|Così questo pilota di drago è la ragazza di Shichimi…
Pepper|10|True|Non posso immaginare cosa sarebbe successo se mi fossi sbarazzata di lei.
Pepper|11|True|E quegli eserciti, devono avermi seguito.
Pepper|12|True|Senza di loro, non saremmo state libere.
Pepper|13|False|Sembra tutto così connesso…
Pepper|14|False|…OH!

### P11

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Torreya|1|False|Ma che le prende?
Shichimi|2|False|Pepper? Va tutto bene?
Pepper|3|True|Si, tutto a posto.
Pepper|4|False|Ho appena capito una cosa.
Pepper|5|True|Tutto quello che è successo è, direttamente o indirettamente, il risultato delle mie azioni e delle mie scelte…
Pepper|6|False|…in altre parole, la mia reazione a catena!
Shichimi|7|True|Davvero?
Shichimi|8|False|Dai dacci una spiegazione.
Torreya|9|True|Basta chiacchiere, siamo nel bel mezzo di una battaglia.
Torreya|10|True|Ne possiamo discutere quando saremo in volo.
Torreya|11|False|Salta su, Pepper!
Shichimi|12|True|Torreya ha ragione.
Shichimi|13|False|In ogni caso dobbiamo andare a Qualicity.
Pepper|14|False|Aspettate un secondo.
Pepper|15|True|L'esercito di Wasabi sta per contrattaccare.
Pepper|16|True|Non possiamo lasciare che si uccidano a vicenda in questo modo.
Pepper|17|False|Sento sia una mia responsabilità porre fine a questa battaglia.

### P12

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Torreya|1|False|Ma come?
Arra|2|True|Si, come ha intenzione di fare, strega?
Arra|3|False|Non hai recuparato abbastanza Rea, lo sento.
Pepper|4|True|Hai perfettamente ragnione, ma c'è un mio incantesimo che potrebbe sistemare ogni cosa.
Pepper|5|False|Ho solo bisogno della tua Rea per poter raggiungere tutti.
Arra|6|True|Dare energia ad una strega?
Arra|7|True|È vietato!
Arra|8|False|Scordatelo! Mai!!
Pepper|9|False|Preferisci piuttosto assistere ad un massacro?
Torreya|10|True|Per favore, Arra. Fai un'eccezione. Quelle ragazze e quei draghi che combattono, sono la nostra scuola, la nostra famiglia.
Torreya|11|False|Ed anche la tua.
Shichimi|12|False|Si, per favore Arra.
Arra|13|True|PFF! Va bene!
Arra|14|False|Ma a suo rischio e pericolo!

### P13

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|False|fs h h h h! !
Pepper|2|False|WOAAAH!
Pepper|3|False|Quindi è così che è la Rea di un drago!
Shichimi|4|False|Pepper, presto! La battaglia!
Pepper|5|True|Allus… !
Pepper|6|True|Yuus… !
Pepper|7|True|Needum… !
Pepper|8|True|Est…
Pepper|9|False|…LOVIUS !
Sound|10|False|Dzziooo!!

### P14

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|True|Fiizz!
Sound|2|True|Dziing!
Sound|3|True|Hfhii!
Sound|4|True|Schii!
Sound|5|True|Schsss!
Sound|6|True|Fiizz!
Sound|7|True|Dziing!
Sound|8|True|Schii!
Sound|9|True|Hfheee!
Sound|10|False|Schsss!
Pepper|11|True|Questo incantesimo è stato la prima prova dei miei incantesimi contro la guerra.
Pepper|12|True|Trasforma nemici mortali in amici…
Pepper|13|False|…e la violenza in amore e compassione.
Shichimi|14|True|Wow!
Shichimi|15|True|Ma… questo è geniale, Pepper!
Shichimi|16|False|Hanno smesso di combattere!
Torreya|17|False|Sta funzionando!

### P15

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|True|Ma cosa sta succedendo?
Shichimi|2|False|Alcuni si stanno baciando…?!
Torreya|3|True|Uh… Ci sono un sacco di nuove coppie!
Torreya|4|False|Era tutto previsto, Pepper?
Pepper|5|False|Oh, no! Penso sia stata la Rea del drago ad amplificare l'amore del mio incantesimo.
Torreya|6|False|Haha, questa battaglia entrerà direttamente nei libri di storia.
Shichimi|7|True|Hee-hee,
Shichimi|8|False|di sicuro!
Pepper|9|False|Ooohhh, che cosa imbarazzante!!
Writing|10|False|- FINE -

### P16

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|False|Lo sapevi?
Pepper|2|False|Pepper&Carrot è completamente libero (gratuito), open-source e sostenuto grazie alle gentili donazioni dei lettori.
Pepper|3|False|Per questo episodio, grazie ai 1036 donatori!
Pepper|4|False|Anche tu puoi diventare un sostenitore di Pepper&Carrot ed avere il tuo nome in questo elenco!
Pepper|5|False|Siamo su Patreon, Tipeee, Paypal, Liberapay …ed altri!
Pepper|6|False|Leggi www.peppercarrot.com per ulteriori informazioni!
Pepper|7|False|Decembere 15, 2021 Disegni e sceneggiatura: David Revoy. Co-autori del testo: Arlo James Barnes, Bhoren, Bobby Hiltz, Craig Maloney, Estefania de Vasconcellos Guimaraes, GunChleoc, Karl Ove Hufthammer, Nicolas Artance, Pierre-Antoine Angelini, Valvin. Versione in italiano Traduzione: Carlo Gandolfi. Correzioni: Antonio Parisi. Basato sull'universo di Hereva Creato da: David Revoy. Amministratore : Craig Maloney. Autori : Craig Maloney, Nartance, Scribblemaniac, Valvin. Correzioni : Willem Sonke, Moini, Hali, CGand, Alex Gryson. Software: Krita 5.0β, Inkscape 1.1 on Kubuntu Linux 20.04. Licenza: Creative Commons Attribution 4.0. www.peppercarrot.com
Pepper|8|False|Grazie mille!
<hidden>|9|False|You can also translate this page if you want.
<hidden>|10|False|NOTE FOR TRANSLATORS Replace this section, e.g.: French version Translation: My Name. Proofreading: Other Name.
<hidden>|11|False|Beta readers help with the story, proofreaders give feedback about the text.
