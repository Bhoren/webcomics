# Transcript of Pepper&Carrot Episode 19 [fr]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titre|1|False|Épisode 19 : Pollution

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cayenne|2|True|Inutile de te surveiller davantage...
Cayenne|3|False|Enterre-moi toutes ces potions ratées et rentre au plus tôt pour te reposer.
Cayenne|4|False|Te voir en réussir une demain dépassera peut-être le stade du rêve, qui sait !
Pepper|5|True|C'est bon, je me dépêche !
Pepper|6|True|Mais d'ailleurs, c'est quoi ce truc de vouloir toujours tout enterrer ?
Pepper|7|False|Ça serait pas mieux si...
Cayenne|8|False|si QUOI ?
Cayenne|1|True|Je rentre.

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
<hidden>|0|False|labels in the garden :-)
Pepper|1|True|Heuu...
Pepper|2|True|Certes, je ne suis pas experte...
Pepper|3|True|Mais cette année notre potager est super bizarre,
Pepper|4|False|et ça concerne aussi pas mal de plantes autour de la maison.
Pepper|7|False|Quant aux fourmis, elles font vraiment des trucs étranges.
Pepper|8|True|Alors, bon...
Pepper|9|False|Je me suis dit, on a peut-être un souci de pollution, et il faudrait sans doute nettoyer tout ça...
Cayenne|10|True|Écoute, mademoiselle Je-Rate-Toutes-Mes-Potions,
Cayenne|12|True|À Chaosah, on enterre nos échecs !
Cayenne|13|False|C'est la tradition depuis la nuit des temps et on se moque de ce qu'en pense DAME NATURE !
Cayenne|14|False|Alors tu te tais et tu CREUSES !!
Cayenne|11|False|ta tenue d'Hippiah doit sans doute te monter à la tête !
Écriture|5|False|Tomates
Écriture|6|False|Aubergines

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|on enterre nos échecs
Pepper|3|False|depuis la nuit des temps
Pepper|2|False|la tradition
Pepper|4|False|Mais OUI !
Pepper|6|False|Allez, plus vite, Carrot !
Carrot|5|False|Zzzz

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
<hidden>|0|False|Diary label on book
Écriture|6|False|Journal Intime de Cayenne
Pepper|1|False|"Oh, beau guerrier blond aux cheveux d'or !"
Pepper|2|False|"...Toi qui hantes mon chaos !"
Pepper|3|False|"...Tu es l'entropie de mes aurores."
Pepper|4|True|C'est fou tout ce qu'on peut apprendre de sorcières qui enterrent tous leurs échecs !
Pepper|5|False|Belle poésie, maître Cayenne !

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cayenne|1|True|Nous avons toutes eu nos échecs.
Cayenne|2|True|Je ne suis plus la Cayenne qui a écrit ça.
Cayenne|3|False|Ce journal a été enterré pour une raison précise.
Pepper|4|False|...
Pepper|5|True|D'accord !
Pepper|6|False|Mais que dites-vous de ceci, hein ?
Thym|8|False|C'est simplement un égarement de jeunesse... Et d'ailleurs, c'est pas de ton âge !
Pepper|9|True|Mm...
Pepper|10|True|Je vois...
Pepper|11|False|Vous n'êtes pas vraiment embarassées par tout ça...
Écriture|7|False|CHAOSAH SUTRA par Thym

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Mais l'environnement ! La nature !
Cayenne|3|False|Ça ne nous a posé aucun problème jusque-là !
Cayenne|5|True|PROFONDÉMENT !
Pepper|2|False|On ne peut pas continuer à polluer tout comme ça sans subir de lourdes conséquences !!!
Cayenne|4|True|Nous sommes des sorcières de Chaosah ! Et nos problèmes, on les enterre
Cayenne|6|False|On ne discute pas les traditions !
Cumin|7|True|Oh, regardez ce que je viens de retrouver !
Cumin|8|False|J'y crois pas !
Cumin|9|False|Comment elle a pu atterrir ici ?
Cumin|10|False|Elle a sans doute besoin d'un petit accordage, mais elle sonne toujours.

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
<hidden>|0|False|Label of materials to translate
Écriture|10|False|verre
Écriture|11|False|métal
Cumin|2|True|Haha, j'ai oublié les paroles !
Cumin|3|False|Sans doute mon livre de chants est par là...
Cumin|1|False|C'était comment déjà ? Cha~Cha Cha, Chaooosah !
Cayenne|4|True|...donc, nous sommes toutes d'accord. Mise à jour des règles de Chaosah :
Cayenne|5|True|désormais, on
Cayenne|6|True|trie,
Cayenne|7|True|broie
Cayenne|8|True|et recycle tout !
Cayenne|9|False|TOUT !!!
Narrateur|12|False|- FIN -
Crédits|13|False|09/2016 - www.peppercarrot.com - Dessin & Scénario : David Revoy
Crédits|14|False|Script doctor: Craig Maloney. Relecture et aide aux dialogues: Valvin, Seblediacre et Alex Gryson. Inspiration: "The book of secrets" by Juan José Segura
Crédits|15|False|Basé sur l'univers d'Hereva créé par David Revoy avec les contributions de Craig Maloney. Corrections de Willem Sonke, Moini, Hali, CGand et Alex Gryson.
Crédits|16|False|Licence : Creative Commons Attribution 4.0, Logiciels: Krita 3.0.1, Inkscape 0.91 sur Arch Linux XFCE

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Crédits|1|False|Pepper&Carrot est entièrement libre, open-source, et sponsorisé grâce au mécénat de ses lecteurs. Pour cet épisode, merci aux 755 Mécènes :
Crédits|2|False|Vous aussi, devenez mécène de Pepper&Carrot sur www.patreon.com/davidrevoy
