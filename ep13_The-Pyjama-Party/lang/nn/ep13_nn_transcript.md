# Transcript of Pepper&Carrot Episode 13 [nn]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Tittel|1|False|Episode 13: Pysjamasfesten

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepar|4|False|Det er berre så kjekt!
Pepar|5|False|Takk for innbydinga, Koriander!
Pepar|3|False|... nokosinne!
Pepar|1|True|Beste ...
Pepar|2|True|... ferien ...

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Koriander|1|True|Takk!
Koriander|2|False|Eg håper staben ikkje er for skremmande.
Pepar|11|False|Nei, slett ikkje ... Dei er svært gjestmilde. Me kjenner oss så velkomne.
Shichimi|13|False|... så raffinert og koseleg på same tid!
Pepar|14|False|Sant!
Shichimi|12|True|Eg er skikkeleg imponert over interiøret ...
Monster|9|False|UuuHuuu ! ! !|nowhitespace
Monster|10|False|Uuu ! ! !|nowhitespace
Monster|8|False|UuuHuuu ! ! !|nowhitespace
Lyd|7|False|svinnnnnng!|nowhitespace
Lyd|6|False|Klaff!|nowhitespace
Lyd|5|False|Klukf!|nowhitespace
Lyd|3|False|Dzzziii !|nowhitespace
Lyd|4|False|Tsjakk !|nowhitespace

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Koriander|2|True|Nokre gongar tenkjer eg det er litt ...
Shichimi|5|True|Eg skulle gjerne budd så komfortabelt,
Lyd|7|False|Skiiingz !|nowhitespace
Lyd|4|False|bam!|nowhitespace
Shichimi|8|False|... «ei-ekte-ah-heks-kan-ikkje-leva-slik».
Pepar|9|True|Ha, ha!
Pepar|10|True|Tradisjonen tyngjer ...
Pepar|11|False|Høyrest ut som gudmødrene mine!
Shichimi|12|False|Verkeleg?
Koriander|1|True|Det er fint å få ei slik påminning.
Koriander|3|False|... ordinært.
Shichimi|6|False|men ...

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Koriander|2|False|Kom igjen, jenter – konsentrer dykk! Me er nesten i mål!
Pepar & Shichimi|1|False|Tji-hi-hi!
Pepar|3|True|Slapp av, det går bra ...
Pepar|5|False|... og me har enno ikkje ...
Lyd|6|False|PfrooOffoff!!!|nowhitespace
Lyd|9|False|Sjssss|nowhitespace
Shichimi|8|False|?!
Koriander|7|False|PEPAR!!!|nowhitespace
Pepar|4|True|Oppdraget er jo superenkelt ...

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Lyd|6|False|K R A K K !|nowhitespace
Shichimi|8|False|Å nei!
Koriander|9|False|Ikkje no igjen!
Pepar|11|False|Slepp han! Med ein gong!
Koriander|1|False|Å nei! Det er for seint! Ho ... Ho er ...
Shichimi|2|False|N e e e i i i!!!|nowhitespace
Monster|3|False|MUAH HAHA HAHA !!!|nowhitespace
Shichimi|4|False|G RR ! ! !|nowhitespace
Lyd|7|False|KLING !|nowhitespace
Lyd|13|False|P LO NK !|nowhitespace
Lyd|12|False|P AFF !|nowhitespace
Pepar|10|True|Gulrot!
Koriander|5|False|DET DER SKAL DU FÅ SVI FOR!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Bidragsytarar|11|False|November 2015 – Teikna og fortald av David Revoy – Omsett av Arild Torvund Olsen og Karl Ove Hufthammer
Lyd|1|False|Dunk|nowhitespace
Pepar|3|True|Kom igjen – ikkje ver furten!
Pepar|5|False|... men det trengst ikkje – det er berre eit spel!
Pepar|7|False|Grrrr|nowhitespace
Skrift|6|False|Festningar og føniksar
Skrift|2|False|Prinsesse Koriander
Forteljar|10|False|– SLUTT –
<hidden>|9|False|Festningar og føniksar
Pepar|4|True|Eg veit du berre vil forsvara meg ...

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Bidragsytarar|1|False|Pepar & Gulrot er ein heilt fri teikneserie, med opne kjelder, og er sponsa av lesarane. Takk til dei 602 som støtta denne episoden:
Bidragsytarar|2|True|Du òg kan støtta arbeidet med neste episode av Pepar og Gulrot:
Bidragsytarar|3|False|https://www.patreon.com/davidrevoy
Bidragsytarar|4|False|Lisens: Creative Commons Attribution 4.0 Kjeldefiler: Tilgjengelege på www.peppercarrot.com Verktøy: Denne episoden er 100 % teikna med fri programvare Krita 2.9.9, Inkscape 0.91 på Linux Mint 17
