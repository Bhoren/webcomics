# Transcript of Pepper&Carrot Episode 11 [nn]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Tittel|1|False|Episode 11: Kaosah-heksene

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Kajenne|1|False|Pepar, du fører skam over kaosah.
Timian|2|False|Kajenne har rett. Ei kaosah-heks som er tittelen verdig, skapar frykt, lydnad og respekt ...
Karve|3|False|... men du, derimot, tilbyr te og muffins – til og med til demonane våre!*
Merknad|4|False|* Sjå episode 8: Pepars fødselsdag
Pepar|5|True|Men ...
Pepar|6|False|... kjære gudmødrer ...

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Kajenne|4|False|STILLE!
Karve|9|False|Ta-daaa!
Timian|1|True|Det er tydeleg at du har talent, men du er òg den einaste som kan ta over etter oss.
Timian|2|False|Me pliktar å gjera deg til ei ekte, fæl kaosah-heks.
Pepar|3|False|Men ... Eg vil ikkje vera fæl! Det ... strir mot alt som eg ...
Kajenne|5|True|... eller så tilbakekallar me alle kreftene dine!
Kajenne|6|False|Og så vert du igjen berre den vesle, dumme, foreldrelause jenta frå Ekornstubben.
Timian|7|False|Heretter vil Karve følgja deg, for å læra deg opp og halda oss orienterte om framdrifta di.
Lyd|8|False|Poff!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Karve|1|False|Den nye kongen av Akren er altfor snill og god med undersåttane sine. Folket skal frykta kongen sin!
Karve|2|False|Som fyrste oppdrag skal du finna og skremma kongen, for lettare å kunna manipulera han.
Karve|3|False|Ei ekte kaosah-heks har makt over dei mektige!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepar|1|True|Kongen ...?
Pepar|2|False|... så ung?!
Pepar|3|False|Me må vera om lag like gamle ...
Karve|4|False|Kva ventar du på?! Kom igjen! Vekk han, og fyll han med skrekk!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Lyd|1|False|Dzzz
Pepar|2|False|Du og eg – me har mykje til felles ...
Pepar|3|True|Unge ...
Pepar|4|True|... åleine ...
Pepar|5|False|... fanga av skjebnane våre ...

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Timian|6|False|Me har mykje arbeid framfor oss, Pepar ...
Bidragsytarar|8|False|September 2015 – Teikna og fortald av David Revoy – Omsett av Karl Ove Hufthammer og Arild Torvund Olsen
Forteljar|7|False|– SLUTT –
Lyd|1|False|Poff!
Pepar|2|False|?!
Lyd|3|False|Floff!
Timian|4|True|Fyrste prøve:
Timian|5|True|STRYK!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Bidragsytarar|1|False|Pepar & Gulrot er ein heilt fri teikneserie, med opne kjelder, og er sponsa av lesarane. Takk til dei 502 som støtta denne episoden:
Bidragsytarar|3|False|https://www.patreon.com/davidrevoy
Bidragsytarar|2|True|Du òg kan støtta arbeidet med neste episode av Pepar og Gulrot:
Bidragsytarar|4|False|Lisens: Creative Commons Attribution 4.0 Kjeldefiler: available at www.peppercarrot.com Verktøy: Denne episoden er 100 % teikna med fri programvare Krita 2.9.7, G’MIC 1.6.5.2, Inkscape 0.91 på Linux Mint 17
