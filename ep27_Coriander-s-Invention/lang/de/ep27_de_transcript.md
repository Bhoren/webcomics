# Transcript of Pepper&Carrot Episode 27 [de]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titel|1|False|Episode 27: Corianders Erfindung

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Erzähler|1|False|Qualicity am Nachmittag
Schneider|4|False|Bitte , Prinzessin Coriander...
Schneider|5|False|Würdet Ihr aufhören, Euch so viel zu bewegen?
Coriander|6|False|Na gut, na gut...
Schneider|7|False|Wir haben kaum mehr Zeit, Eure Krönung beginnt in drei Stunden.
Coriander|8|False|...
Schrift|2|True|Krönung Ihrer Majestät
Schrift|3|False|Königin CORIANDER
<hidden>|0|False|Translate “Her Majesty’s Coronation” and “Queen Coriander” on the banner on the building (in the middle). The other text boxes will update automatically (they are clones).
<hidden>|0|False|NOTE FOR TRANSLATORS
<hidden>|0|False|You can strech the scroll and translate “Qualicity” to “City of Qualicity”.
<hidden>|0|False|NOTE FOR TRANSLATORS

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Coriander|1|True|Wisst ihr...
Coriander|2|False|Ich habe wieder über die Sicherheitsmassnahmen nachgedacht...
Coriander|3|True|So viele junge Könige und Königinnen wurden während ihrer Krönung ermordet.
Coriander|4|False|Es ist teil der Geschichte meiner Familie geworden.
Coriander|5|True|Und nun werden wir in einigen Stunden tausende Gäste aus der ganzen Welt empfangen und alle Aufmerksamkeit wird auf mich gerichtet sein.
Pepper|7|True|Mach dir keine Sorgen, Coriander.
Coriander|6|False|Ich habe solche Angst!
Pepper|8|True|Wir haben einen Plan.
Pepper|9|False|Shichimi und ich werden deine Leibwächter sein, nicht wahr?
Shichimi|10|True|Genau.
Shichimi|11|False|Du solltest eine Pause machen und an etwas anderes denken.
Coriander|12|False|OK.
Coriander|13|False|Ich weiss was! Ich könnte euch meine neuste Erfindung zeigen.
Schneider|14|False|...
Coriander|15|True|Es ist unten, in der Werkstatt.
Coriander|16|False|Mir nach!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Coriander|1|False|Ihr werdet gleich sehen, weshalb ich daran gearbeitet habe.
Coriander|2|False|Es hat mit meinen Sorgen wegen der Krönung zu tun.
Coriander|3|False|Ich präsentiere: Meine neuste Erfindung...
Roboter-Psychologe|5|False|Hallo Welt.
Roboter-Psychologe|6|False|Ich bin dein ROBOTER-PSYCHOLOGE biep, biep!
Roboter-Psychologe|7|False|Bitte lege dich auf die Couch für eine kostenlose Therapie.
Geräusch|4|False|Klick!
Geräusch|8|False|Tapp!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Coriander|1|True|Na, was denkt ihr?
Coriander|2|False|Wollt ihr es mal versuchen?
Pepper|3|True|Bah.
Pepper|4|True|Nicht nötig.
Pepper|5|False|Ich habe keine psychologischen Probleme.
Geräusch|6|True|T|nowhitespace
Pepper|11|False|HE, WAS STIMMT NICHT MIT DEINEM ROBOTER?!
Coriander|12|True|Entschuldige!
Coriander|13|True|Er hat immer noch den einen Fehler, den ich nicht beheben konnte.
Coriander|14|False|Er gibt einen Strom-stoss ab, sobald er eine Lüge erkennt.
Pepper|15|False|Eine Lüge?!
Pepper|16|False|Ich habe in meinem ganzen LEBEN noch nie gelogen!
Pepper|22|True|DAS...
Pepper|23|True|IST NICHT...
Pepper|24|False|LUSTIG!!!
Geräusch|7|True|Z|nowhitespace
Geräusch|8|True|Z|nowhitespace
Geräusch|9|True|Z|nowhitespace
Geräusch|10|False|!|nowhitespace
Geräusch|17|True|T
Geräusch|18|True|Z|nowhitespace
Geräusch|19|True|Z|nowhitespace
Geräusch|20|True|Z|nowhitespace
Geräusch|21|False|!|nowhitespace

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|False|Lustig? Nein, natürlich nicht! Entschuldige.
Coriander|2|False|Ha ha! Stimmt! Überhaupt nicht lustig!
Pepper|8|True|Ha...
Pepper|9|True|Ha...
Pepper|10|False|Ha.
Coriander|11|True|OH NEIN!
Coriander|12|False|Mein Kleid für die Zeremonie ist ruiniert! Der Schneider wird durchdrehen!
Coriander|13|True|Böser Roboter!
Coriander|14|False|Ich werde dich reparieren... Mit dem Hammer!
Pepper|15|False|WARTE!!!
Geräusch|3|True|T
Geräusch|4|True|Z|nowhitespace
Geräusch|5|True|Z|nowhitespace
Geräusch|6|True|Z|nowhitespace
Geräusch|7|False|!|nowhitespace

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Ich habe eine Idee, wie wir diesen Fehler nutzen können.
Geräusch|2|False|Kling!
Erzähler|3|False|Am Abend...
Coriander|11|False|Pepper, du bist ein Genie!
Pepper|12|True|Nein, du bist's.
Pepper|13|False|Es ist deine Erfindung!
Roboter-Psychologe|14|False|NÄCHSTER, BITTE.
Roboter-Psychologe|15|True|HALLO,
Roboter-Psychologe|16|False|HABEN SIE HEUTE ABEND EIN ATTENTAT GEPLANT?
Erzähler|17|False|FORTSETZUNG FOLGT…
Geräusch|6|True|T
Geräusch|7|True|Z|nowhitespace
Geräusch|8|True|Z|nowhitespace
Geräusch|9|True|Z|nowhitespace
Geräusch|10|False|!|nowhitespace
Schrift|4|True|Krönung Ihrer Majestät
Schrift|5|False|Königin CORIANDER

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|5|True|Du kannst auch Spender von Pepper&Carrot werden und deinen Namen hier lesen!
Pepper|3|True|Pepper&Carrot ist vollständig frei(libre), Open Source und finanziert durch Spenden von Lesern.
Pepper|4|False|Für diese Episode danken wir 1060 Spendern!
Pepper|7|True|Geh auf www.peppercarrot.com für mehr Informationen!
Pepper|6|True|Wir sind auf Patreon, Tipeee, PayPal, Liberapay ...und mehr!
Pepper|8|False|Dankeschön!
Pepper|2|True|Wusstest du schon?
Impressum|1|False|31. Oktober, 2018 Illustration & Handlung: David Revoy. Beta-Leser: Amyspark, Butterfly, Bhoren, CalimeroTeknik, Craig Maloney, Gleki Arxokuna, Imsesaok, Jookia, Martin Disch, Midgard, Nicolas Artance, uncle Night, ValVin, xHire, Zveryok. Deutsche Version Übersetzung: Martin Disch . Basierend auf der Hereva-Welt Erstellung: David Revoy. Hauptbetreuer: Craig Maloney. Schreiber: Craig Maloney, Nartance, Scribblemaniac, Valvin. Korrektur: Willem Sonke, Moini, Hali, CGand, Alex Gryson . Software: Krita 4.2-dev, Inkscape 0.92.3 auf Kubuntu 18.04. Lizenz: Creative Commons Namensnennung 4.0. www.peppercarrot.com
<hidden>|0|False|NOTE FOR TRANSLATORS Replace this section, e.g.: French version Translation: My Name. Proofreading: Other Name.
<hidden>|0|False|You can also translate this page if you want.
<hidden>|0|False|Beta readers help with the story, proofreaders give feedback about the text.
