# Transcript of Pepper&Carrot Episode 31 [fr]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titre|1|False|Épisode 31 : Le Combat

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Narrateur|1|False|Ténébrume, Colline sacrée de Chaosah.
Son|2|False|VRioo o !|nowhitespace
Pepper|3|False|Tss !
Son|4|False|VRioo ooo|nowhitespace
Son|5|False|Bzi ooo !|nowhitespace
Pepper|6|False|Prends ça !
Son|8|False|Schh !
Cayenne|9|False|Amateure !
Pepper|10|False|! !|nowhitespace
Son|11|False|V L AN !|nowhitespace

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Son|1|False|Dzzzioo oo !|nowhitespace
Son|2|False|CR AC ! !!|nowhitespace
Son|3|False|CR A A C ! !!|nowhitespace
Pepper|4|False|Pas si vite !!!
Pepper|5|False|BOUCLIUS GRAVITATIONAS !
Son|6|False|DZZzioo ! !|nowhitespace
Son|7|False|Tchkshkk ! !|nowhitespace
Son|8|False|Tchkshkk ! !|nowhitespace
Son|9|False|Toc!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|CARROT !
Pepper|2|False|Plan 7-B !
Pepper|4|False|JPEGUS QUALITIS !
Son|5|False|Bzi ooo !|nowhitespace
Son|3|False|Zo u uu|nowhitespace
Cayenne|6|False|?!!
Cayenne|10|False|Argh !!
Son|7|True|G
Son|8|True|Z|nowhitespace
Son|9|False|Z|nowhitespace
Pepper|11|False|QUALITIS MINIMALIS !
Cayenne|12|False|! !|nowhitespace
Cayenne|13|False|Grr...
Écriture|14|False|2019-12-20-E31P03_V15-final.jpg
Écriture|15|False|Erreur lors du chargement
Son|16|False|CRASH ! !!|nowhitespace

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cayenne|1|False|PYRO BOMBA ATOMICUS !
Son|2|False|Fr rrooou !|nowhitespace
Son|3|True|K
Son|4|True|A|nowhitespace
Son|5|True|B|nowhitespace
Son|6|True|O|nowhitespace
Son|7|True|O|nowhitespace
Son|8|True|M|nowhitespace
Son|9|True|!|nowhitespace
Son|10|False|!|nowhitespace
Son|11|True|BRRR
Son|12|False|BRRR
Son|13|False|Sch hh hh...|nowhitespace

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cayenne|1|False|Pfff...
Cayenne|2|True|Fuir dans une microdimension à ton âge?
Cayenne|3|False|Quelle pathétique défaite ...
Pepper|4|True|Perdu !
Pepper|5|False|Un trou de ver !
Son|6|False|Bzz ! !|nowhitespace
Pepper|7|False|Merci Carrot pour la sortie !
Pepper|8|False|Échec et mat maître Cayenne !
Pepper|9|False|GURGES...
Pepper|10|False|...ATER !
Son|12|False|Swwwwwwiiiiiiiipppp !!!
Son|11|False|V R O O O ! !!|nowhitespace
Thym|13|False|STOP !!!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Thym|1|True|J'ai dit : STOP !
Thym|2|False|Ça suffit !
Son|3|False|Clack !|nowhitespace
Son|4|False|P CHAC !|nowhitespace
Son|5|False|TCHK !
Thym|6|True|Vous deux !
Thym|7|True|ICI !
Thym|8|False|Immédiatement !

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Son|1|False|Toc !
Son|2|False|Toc !
Thym|3|True|Ça fait trois ans que tu nous fais le coup d'une « vérification de dernière minute »...
Thym|4|False|...on va pas y passer toute la nuit !
Thym|5|False|Alors... ?
Pepper|6|False|...
Cayenne|7|True|OK...
Cayenne|8|True|...Elle peut avoir son diplôme...
Cayenne|9|False|... mais mention « passable ».
Écriture|10|True|Diplôme
Écriture|11|True|de
Écriture|12|False|Chaosah
Écriture|14|False|Cayenne
Écriture|13|False|Cumin
Écriture|15|False|T h ym|nowhitespace
Écriture|16|False|~ pour Pepper ~
Écriture|17|False|Sorcière officielle
Narrateur|18|False|- FIN -

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|5|True|Vous pouvez aussi devenir mécène de Pepper&Carrot et avoir votre nom inscrit ici !
Pepper|3|True|Pepper&Carrot est entièrement libre, gratuit, open-source et sponsorisé grâce au mécénat de ses lecteurs.
Pepper|4|False|Cet épisode a reçu le soutien de 971 mécènes !
Pepper|7|True|Allez sur www.peppercarrot.com pour plus d'informations !
Pepper|6|True|Nous sommes sur Patreon, Tipeee, PayPal, Liberapay ... et d'autres !
Pepper|8|False|Merci !
Pepper|2|True|Le saviez-vous?
Crédits|1|False|Le 20 décembre 2019 Art & scénario : David Revoy. Lecteurs de la version bêta : Craig Maloney, Martin Disch, Arlo James Barnes, Nicolas Artance, Valvin. Version française originale Relecture : Nicolas Artance, Valvin . Basé sur l'univers d'Hereva Créateur : David Revoy. Mainteneur principal : Craig Maloney. Rédacteurs : Craig Maloney, Nartance, Scribblemaniac, Valvin. Correcteurs : Willem Sonke, Moini, Hali, CGand, Alex Gryson . Logiciels : Krita 4.2.6appimage, Inkscape 0.92.3 on Kubuntu 18.04-LTS. Licence : Creative Commons Attribution 4.0. www.peppercarrot.com
<hidden>|0|False|NOTE FOR TRANSLATORS Replace this section, e.g.: French version Translation: My Name. Proofreading: Other Name.
<hidden>|0|False|You can also translate this page if you want.
<hidden>|0|False|Beta readers help with the story, proofreaders give feedback about the text.
