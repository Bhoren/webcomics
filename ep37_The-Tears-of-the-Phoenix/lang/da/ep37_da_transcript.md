# Transcript of Pepper&Carrot Episode 37 [da]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titel|1|False|Episode 37: Føniksens tårer

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Ah...
Pepper|2|False|Det er dejligt at være ankommet!
Pepper|3|False|Nå, det er vist markedsdag.
Pepper|4|False|Vi burde nok spise lidt før opstigningen, ik'?
Pepper|5|False|Jeg var sikker på, du ville kunne lide idéen!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Skrift|1|False|EFTERLYSTE
Skrift|2|False|Torreya
Skrift|3|False|1 00 000Ko
Skrift|4|False|Shichimi
Skrift|5|False|250 000Ko
Skrift|6|False|Pepper
Skrift|7|False|1 000 000Ko

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|De har virkelig hængt dem op overalt...
Pepper|2|False|Selv i de fjerneste afkroge.
Pepper|3|False|Kom, lad os smutte før vi vækker opmærksomhed.
Pepper|4|False|Vi har meget større bekymringer lige nu...
Pepper|5|False|Det lys...
Pepper|6|False|Vi må være tæt på dens rede.
Pepper|7|False|Bingo!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Phoenix|1|False|Hvorfor kommer du og forstyrrer mig, menneske?
Pepper|2|False|Vær hilset, O store Føniks!
Pepper|3|False|Jeg hedder Pepper, og jeg er Kaosah-heks.
Pepper|4|False|Jeg har for nylig modtaget en kæmpe dosis drage-Rea, og siden da er det her dukket op, og vokser dag efter dag...
Pepper|5|False|Det neutraliserer alle mine kræfter og kan ende med at tage livet af mig.

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Phoenix|1|True|Hmm...
Phoenix|2|False|Jaså...
Pepper|3|False|...og nu befinder du dig her, fordi du vil have Føniks-tårer, så du kan helbredes, ikke sandt?
Pepper|4|False|Ja, det er min eneste mulighed.
Phoenix|5|False|*suk*
Phoenix|6|False|... nå, hvad venter du på?
Phoenix|7|False|Prøv så at få mig til at græde.
Phoenix|8|False|Jeg giver dig et minut !
Pepper|9|False|Hvad?!
Pepper|10|False|Få Dem til at græde?!
Pepper|11|False|Men jeg vidste ikke at...
Pepper|12|False|Og jeg har kun ét minut ?!
Pepper|13|True|Øhm...
Pepper|14|True|OK!
Pepper|15|False|Lad os se.
Pepper|16|True|Hmm... Tænk på verdens hungersnød .
Pepper|17|True|Øh, nej nej nej!
Pepper|18|True|Jeg har noget bedre:
Pepper|19|False|Dem De har mistet .
Pepper|20|True|Stadig ikke?
Pepper|21|True|Og forladte kæledyr ?!
Pepper|22|False|Det er så sørgeligt, når kæledyr bliver forladt...
Phoenix|23|False|...

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Nå?
Pepper|2|False|Får det Dem ikke til at græde?
Phoenix|3|False|ER DU SERIØS?!!
Phoenix|4|False|ER DET ALT DET DU HAR?!!
Phoenix|5|True|De har i det mindste prøvet poesi!
Phoenix|6|True|Skrevet tragedier!
Phoenix|7|True|KUNST!
Phoenix|8|False|DRAMA!
Phoenix|9|True|OG DU?!
Phoenix|10|False|DU KOMMER UFORBEREDT!
Phoenix|11|True|JA, SMUT DU BARE!
Phoenix|12|False|GÅ HJEM, OG KOM TILBAGE NÅR DU HAR NOGET BEDRE!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Grrr! Kom nu Pepper! Find en sørgelig historie.
Pepper|2|True|Du kan klare det.
Pepper|3|False|Du kan klare det.
Pepper|4|True|Nej...
Pepper|5|False|Du kan ikke.
Sælger|6|False|Hey! Dig dér! Gå væk!
Pepper|7|False|! !
Sælger|8|False|Rør ikke ved mine varer, hvis du ik' ka' betale, okay?
Pepper|9|True|Åh, nej...
Pepper|10|True|CARROT!
Pepper|11|False|Kunne du ikke hjælpe mig i stedet for at tænke på din mave?
Pepper|12|False|Åh?!
Pepper|13|True|Aha...
Pepper|14|False|Det burde virke.

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Phoenix|1|True|Ah! Du er tilbage.
Phoenix|2|False|Det var hurtigt...
Phoenix|3|False|Et metalvåben?!
Phoenix|4|False|Virkelig?!
Phoenix|5|False|Ved du da ikke, at jeg kan smelte al slags metal, og...
Lyd|6|False|Plop
Lyd|7|False|Plop
Lyd|8|False|Plop
Phoenix|9|True|ÅH NEJ!
Phoenix|10|False|IKKE DET!
Phoenix|11|False|DET ER IKKE FAIR!
Pepper|12|True|Skynd dig Carrot!
Pepper|13|False|Fang så mange tårer som muligt!
Lyd|14|False|Hak!
Lyd|15|False|Hak!
Lyd|16|False|Hak!
Titel|17|False|- SLUT -

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titel|1|False|Den 3. august 2022 Tegning og manuskript: David Revoy. Genlæsning i beta-versionen: Arlo James Barnes, Benjamin Loubet, Bobby Hiltz, Chloé, Craig Maloney, Estefania de Vasconcellos Guimaraes, GunChleoc, Nicolas Artance, Olivier Jolly, Rhombihexahedron, Valvin. Dansk Version Oversættelse: Emmiline Alapetite Rettelser: Rikke & Alexandre Alapetite, Troels Bech Gravgaard Møller, Tobias Hinnerup Baseret på Hereva-universet Skabt af: David Revoy. Vedligeholdt af: Craig Maloney. Medforfattere: Craig Maloney, Nicolas Artance, Scribblemaniac, Valvin. Rettelser: Willem Sonke, Moini, Hali, CGand, Alex Gryson. Værktøj: Krita 5.0.5appimage, Inkscape 1.2 on Kubuntu Linux 20.04 Licens: Creative Commons Attribution 4.0. www.peppercarrot.com
Pepper|2|False|Vidste I det?
Pepper|3|False|Pepper & Carrot er fri, open-source og sponsoreret af sine læsere.
Pepper|4|False|Denne episode blev støttet af 1058 tilhængere!
Pepper|5|False|Du kan også blive tilhænger og få dit navn skrevet her!
Pepper|6|False|Vi er på Patreon, Tipeee, PayPal, Liberapay … og andre!
Pepper|7|False|Gå på www.peppercarrot.com og få mere information!
Pepper|8|False|Tak!
Notes|9|False|You can also translate this page if you want.
Notes|10|False|NOTE FOR TRANSLATORS Replace this section, e.g.: French version Translation: My Name. Proofreading: Other Name.
Notes|11|False|Beta readers help with the story, proofreaders give feedback about the text.
