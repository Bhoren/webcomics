# Transcript of Pepper&Carrot Episode 37 [kw]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|Rann 37: Dagrow an Feniks

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Wortiwedh...
Pepper|2|False|Ass yw da bos omma!
Pepper|3|False|Ogh, dydh marghas yw.
Pepper|4|False|Gwren synsi temmik dhe dhybri kyns yskynna an loskvenydh.
Pepper|5|False|Y hwodhvev ty dhe gara an tybyans na!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Writing|1|False|HWILYS
Writing|2|False|Torreya
Writing|3|False|1 00 000Ko
Writing|4|False|Shichimi
Writing|5|False|250 000Ko
Writing|6|False|Pepper
Writing|7|False|1 000 000Ko

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|I re bostyas hemma yn pub le yn tevri.
Pepper|2|False|Y'n pella kornellow hogen.
Pepper|3|False|Deus yn-rag, gwren diberth kyns ni dhe vos aswonys.
Pepper|4|False|Ni a'gan beus kudynnow brassa lemmyn...
Pepper|5|False|An golow na...
Pepper|6|False|Sur ov ni dhe vos ogas dh'y neyth.
Pepper|7|False|Sewena!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Phoenix|1|False|Prag yth esos orth ow ankombra, A vowes?
Pepper|2|False|Dynarghow, A Feniks meur!
Pepper|3|False|Pepper yw ow hanow, ha gwragh a Chaosah ov vy.
Pepper|4|False|A-gynsow y tegemeris dogen kowrek Rea Dragon ha wosa henna, hemma re dheuth ha pesya tevi pub dydh...
Pepper|5|False|Ev a wra dinertha oll ow nerthow hag y halsa ow ladha wortiwedh.

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Phoenix|1|True|Hmm...
Phoenix|2|False|My a wel...
Pepper|3|False|...hag yth esos omma lemmyn drefen bos edhom dhis a dhagrow Feniks rag dha yaghhe, gwir?
Pepper|4|False|Gwir, nyns eus dewis aral dhymm.
Phoenix|5|False|*hanas*
Phoenix|6|False|...well, pandr'a wortydh?
Phoenix|7|False|Assay dhe wul dhymm ola.
Phoenix|8|False|My a re dhis mynysen!
Pepper|9|False|Pyth?!
Pepper|10|False|Gul dhis ola?!
Pepper|11|False|Mes ny wodhvev henna...
Pepper|12|False|My a styr... Saw unn vynysen?!
Pepper|13|True|Um...
Pepper|14|True|OK!
Pepper|15|False|Gwelyn.
Pepper|16|True|Hmm... Tyb a-dro dhe nown bysel.
Pepper|17|True|Er, gortagorta!
Pepper|18|True|My a'm beus neppyth gwell:
Pepper|19|False|An re a gelsys.
Pepper|20|True|Tra vyth hwath?
Pepper|21|True|Enevales dov forsakyes?!
Pepper|22|False|Ass yw forsakyans eneval dov trist...
Phoenix|23|False|...

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Ytho, pandr'a hwer?
Pepper|2|False|A ny vyn'ta ola?
Phoenix|3|False|OS TA SAD?!
Phoenix|4|False|YW HENNA OLL EUS GENES?!
Phoenix|5|True|I a assayas bardhonieth, dhe'n lyha!
Phoenix|6|True|Skrifa trajedis!
Phoenix|7|True|ART!
Phoenix|8|False|DRAMA!
Phoenix|9|True|MES TY?!
Phoenix|10|False|TY A DHEU HEB OMBAREUSI!
Phoenix|11|True|YA, GWRA YNDELLA: KE!
Phoenix|12|False|KE DHE'TH TRE HA DEHWELES PAN VI TA PARYS!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Grr! War yew, Pepper! Kav hwedhel trist.
Pepper|2|True|Y hyllydh y wul.
Pepper|3|False|Y hyllydh y wul.
Pepper|4|True|Na...
Pepper|5|False|Ny yllydh.
Vendor|6|False|Dar! Ogh! Ke dhe-ves!
Pepper|7|False|!!
Vendor|8|False|Gwith dha bawyow dhyworth ow thaklow mar ny yllydh pe!
Pepper|9|True|Na, na wra henna...
Pepper|10|True|CARROT!
Pepper|11|False|Ty a alsa ow gweres yn le tybi a-dro dhe'th torr tejy!
Pepper|12|False|O?!
Pepper|13|True|Ogh, ya...
Pepper|14|False|Hemma a alsa oberi.

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Phoenix|1|True|A! Dehwelys os ta.
Phoenix|2|False|Mar skon...
Phoenix|3|False|Arv alkan?!
Phoenix|4|False|Yn hwir?!
Phoenix|5|False|A ny wodhes y hallav teudhi pub alkan oll ha...
Sound|6|False|Plopp
Sound|7|False|Plopp
Sound|8|False|Plopp
Phoenix|9|True|OGH NA!
Phoenix|10|False|NA HENNA!
Phoenix|11|False|NYNS YW RESNADOW!
Pepper|12|True|Fysk Carrot!
Pepper|13|False|Kach mar lies dager dell yllydh!
Sound|14|False|Nadh!
Sound|15|False|Nadh!
Sound|16|False|Nadh!
Title|17|False|- FIN -

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|A wodhes?
Pepper|2|False|Pepper&Carrot yw rydh (libre) yn tien, fenten-ygor ha skoodhys dre weres kuv y redyoryon.
Pepper|3|False|Rag an rann ma, synsys on dhe'n 1058 tasek!
Pepper|4|False|Y hyllir dos ha bos tasek Pepper&Carrot ha kavos dha hanow omma!
Pepper|5|False|Yth eson war Patreon, Tipeee, PayPal, Liberapay ... ha moy!
Pepper|6|False|Mir orth www.peppercarrot.com rag derivadow moy!
Pepper|7|False|Meur ras!
Pepper|8|False|2022-08-03 Art & hwedhel: David Revoy. Redyoryon beta: Arlo James Barnes, Benjamin Loubet, Bobby Hiltz, Chloé, Craig Maloney, Estefania de Vasconcellos Guimaraes, GunChleoc, Nicolas Artance, Olivier Jolly, Rhombihexahedron, Valvin. Treylyans Kernewek: Steve Harris. Provredyans: Steve Penhaligon. Selys war an ollvys a Hereva Gwrier: David Revoy. Pennventenour: Craig Maloney. Skriforyon: Craig Maloney, Nicolas Artance, Scribblemaniac, Valvin. Correctors: Willem Sonke, Moini, Hali, CGand, Alex Gryson. Medhelweyth: Krita 5.0.5, Inkscape 1.2 war Fedora 36 KDE Spin. Kummyas: Creative Commons Attribution 4.0. www.peppercarrot.com
Notes|9|False|You can also translate this page if you want.
Notes|10|False|NOTE FOR TRANSLATORS Replace this section, e.g.: French version Translation: My Name. Proofreading: Other Name.
Notes|11|False|Beta readers help with the story, proofreaders give feedback about the text.
