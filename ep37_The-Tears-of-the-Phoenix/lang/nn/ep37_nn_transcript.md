# Transcript of Pepper&Carrot Episode 37 [nn]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Tittel|1|False|Episode 37: Fønikstårene

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepar|1|True|Puh ...
Pepar|2|False|Endeleg framme!
Pepar|3|False|Det er visst marknads-dag i dag.
Pepar|4|False|La oss få oss ein matbit før me går opp på vulkanen.
Pepar|5|False|Eg tenkte du ville lika den planen!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Skrift|1|False|ETTERLYSTE
Skrift|2|False|Torreya
Skrift|3|False|1 00 000 Ko
Skrift|4|False|Shichimi
Skrift|5|False|250 000 Ko
Skrift|6|False|Pepar
Skrift|7|False|1 000 000 Ko

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepar|1|True|Dei har verkeleg hengt desse overalt.
Pepar|2|False|Til og med på dei mest avsidesliggjande stadane.
Pepar|3|True|Kom, me bør stikka før nokon kjenner oss att.
Pepar|4|False|Me har verre ting å plagast med for tida ...
Pepar|5|True|Det lyset ...
Pepar|6|False|Reiret kan ikkje vera langt unna.
Pepar|7|False|Bingo!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Føniks|1|False|Kvifor trengjer du deg på, menneske?
Pepar|2|False|Ver helsa, o store føniks!
Pepar|3|False|Eg heiter Pepar og er kaosah-heks.
Pepar|4|False|Nyleg vart eg utsett for ein stor dose drake-rea, og sidan då har eg hatt dette her, som vert større dag for dag ...
Pepar|5|False|Det set kreftene mine ut av spel – og kan med tida ta livet av meg!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Føniks|1|True|Hm ...
Føniks|2|False|Eg skjønar ...
Føniks|3|False|Og no er du her fordi du treng ei fønikståre til å kurera deg, eller?
Pepar|4|False|Ja, det er det einaste håpet mitt.
Føniks|5|False|*sukk*
Føniks|6|False|Vel, kva ventar du på?
Føniks|7|False|Prøv å få meg til å gråta.
Føniks|8|False|Du har eitt minutt på deg.
Pepar|9|False|Kva?!
Pepar|10|False|Få deg til å gråta?!
Pepar|11|False|Men eg visste ikkje at ...
Pepar|12|False|Altså ... På eitt minutt?!
Pepar|13|True|Æh ...
Pepar|14|True|OK!
Pepar|15|False|La meg sjå.
Pepar|16|False|Hm ... Tenk på hungersnaud.
Pepar|17|True|Æh, vent-vent-vent!
Pepar|18|True|No kom eg på noko betre:
Pepar|19|False|Dei du har mista.
Pepar|20|False|Framleis ingenting?
Pepar|21|True|Forlatne kjæledyr?!
Pepar|22|False|Det er så trist med dyr som vert forlatne ...
Føniks|23|False|...

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepar|1|True|No, kva vert det til?
Pepar|2|False|Kjenner du ikkje tårene pressa på?
Føniks|3|False|MEINER DU ALVOR?!
Føniks|4|False|ER DETTE DET BESTE DU FÅR TIL?!
Føniks|5|True|Dei der prøvde i det minste med dikt!
Føniks|6|True|Dei skreiv tragediar!
Føniks|7|True|KUNST!
Føniks|8|False|DRAMA!
Føniks|9|True|MEN DU?!
Føniks|10|False|MØTER OPP HEILT UBUDD!
Føniks|11|True|JA, NETTOPP – HUSJ MED SEG!
Føniks|12|False|DRA HEIM OG KOM IKKJE ATT FØR DU ER KLAR!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepar|1|True|Grrr! Kom igjen, Pepar! Tenk på noko trist.
Pepar|2|False|Dette greier du.
Pepar|3|False|Dette greier du.
Pepar|4|True|Nei ...
Pepar|5|False|Det gjer du ikkje.
Grønsakshandlar|6|False|Hei der! Vekk med deg!
Pepar|7|False|!!!
Grønsakshandlar|8|False|Hald potane av fatet om du ikkje kan betala!
Pepar|9|True|Å nei, kva har du ...
Pepar|10|True|GULROT!
Pepar|11|False|Kva med å hjelpa i staden for berre å tenkja med magen?!
Pepar|12|False|Å?!
Pepar|13|True|Nettopp ...
Pepar|14|False|Det kan fungera.

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Føniks|1|True|Så du er tilbake.
Føniks|2|False|Det gjekk fort ...
Føniks|3|True|Eit metall-våpen?!
Føniks|4|False|Verkeleg?!
Føniks|5|False|Veit du ikkje at eg kan smelta alle typar metall og ...
Lyd|6|False|Plopp
Lyd|7|False|Plopp
Lyd|8|False|Plopp
Føniks|9|True|Å NEI!
Føniks|10|True|IKKJE DET!
Føniks|11|False|DET ER JUKS!
Pepar|12|True|Skund deg, Gulrot!
Pepar|13|False|Fang alle tårene du klarar!
Lyd|14|False|Hogg!
Lyd|15|False|Hogg!
Lyd|16|False|Hogg!
Tittel|17|False|– SLUTT –

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepar|3|True|Pepar & Gulrot er ein heilt fri teikneserie, med opne kjelder, og er sponsa av lesarane.
Pepar|4|False|Takk til dei 1 058 som støtta denne episoden!
Pepar|2|True|Visste du dette?
Bidragsytarar|1|False|2022-08-03 Teikningar og forteljing: David Revoy. Tidleg tilbakemelding: Arlo James Barnes, Benjamin Loubet, Bobby Hiltz, Chloé, Craig Maloney, Estefania de Vasconcellos Guimaraes, GunChleoc, Nicolas Artance, Olivier Jolly, Rhombihexahedron og Valvin. Omsetjing til nynorsk Karl Ove Hufthammer og Arild Torvund Olsen. Bygd på Hereva-universet Skapar: David Revoy. Hovudutviklar: Craig Maloney. Forfattarar: Craig Maloney, Nicolas Artance, Scribblemaniac og Valvin. Rettingar: Willem Sonke, Moini, Hali, CGand og Alex Gryson. Programvare: Krita 5.0.5 og Inkscape 1.2 på Fedora 36 KDE Spin. Lisens: Creative Commons Attribution 4.0. www.peppercarrot.com
Pepar|5|True|Du òg kan støtta arbeidet med neste episode av Pepar & Gulrot og få namnet ditt her!
Pepar|7|True|Sjå www.peppercarrot.com for meir informasjon!
Pepar|6|True|Me er på blant anna Patreon, Tipeee, PayPal og Liberapay ...
Pepar|8|False|Tusen takk!
<hidden>|9|False|You can also translate this page if you want.
<hidden>|10|False|NOTE FOR TRANSLATORS Replace this section, e.g.: French version Translation: My Name. Proofreading: Other Name.
<hidden>|11|False|Beta readers help with the story, proofreaders give feedback about the text.
