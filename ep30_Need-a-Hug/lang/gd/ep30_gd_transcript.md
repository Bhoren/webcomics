# Transcript of Pepper&Carrot Episode 30 [gd]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Tiotal|1|False|Eapasod 30: Glac mi teann

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Neach-aithris|1|False|- Deireadh na sgeòil -

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Peabar|5|True|’S urrainn dhut dol ’nad phàtran Pepper&Carrot cuideachd is chì thu d’ àinm an-seo!
Peabar|3|True|Tha Pepper&Carrot saor is le tùs fosgailte air fad agus ’ga sponsaireadh le taic nan leughadairean.
Peabar|4|False|Mòran taing dhan 973 pàtran a thug taic dhan eapasod seo!
Peabar|7|True|Tadhail air www.peppercarrot.com airson barrachd fiosrachaidh!
Peabar|6|True|Tha sinn air Patreon, Tipeee, PayPal, Liberapay ...’s a bharrachd!
Peabar|8|False|Mòran taing!
Peabar|2|True|An robh fios agad?
Urram|1|False|3s dhen t-Sultain 2019 Obair-ealain ⁊ sgeulachd: David Revoy. Leughadairean Beta a’ bhùird-stòiridh: Alina the Hedgehog, Craig Maloney, Jihoon Kim, Parnikkapore, Martin Disch, Nicolas Artance, Valvin. Tionndadh Gàidhlig Eadar-theangachadh: GunChleoc. Stèidhichte air saoghal Hereva Air a chruthachadh le: David Revoy. Prìomh neach-glèidhidh: Craig Maloney. Sgrìobhadairean: Craig Maloney, Nicolas Artance, Scribblemaniac, Valvin. Ceartachadh: Willem Sonke, Moini, Hali, CGand, Alex Gryson . Bathar-bog: Krita/4.2~git branch, Inkscape 0.92.3 air Kubuntu 18.04.2. Ceadachas: Creative Commons Attribution 4.0. www.peppercarrot.com
<hidden>|0|False|NOTE FOR TRANSLATORS Replace this section, e.g.: French version Translation: My Name. Proofreading: Other Name.
<hidden>|0|False|You can also translate this page if you want.
<hidden>|0|False|Beta readers help with the story, proofreaders give feedback about the text.
