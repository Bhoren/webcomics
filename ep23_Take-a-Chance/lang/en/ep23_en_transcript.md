# Transcript of Pepper&Carrot Episode 23 [en]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|Episode 23: Take a Chance

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Narrator|1|False|Komona Market, one week later
Writing|2|False|Celebrity
Writing|3|False|50 000 Ko
Writing|4|False|Saffron's Success
Writing|5|False|Fashion
Writing|6|False|The Saffron Phenomenon
Writing|7|False|Chic
Writing|8|False|Saffron Special
Sound|9|False|P A F !|nowhitespace

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Thanks, Carrot…
Pepper|2|False|You know, I work hard every day to be a good witch…
Pepper|3|True|…respecting the traditions, the rules…
Pepper|4|False|…it's not fair that Saffron should become so famous with those kinds of tactics.
Pepper|5|False|It's really unfair.

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Genie of Success|2|True|Hello!
Genie of Success|3|False|Please allow me to introduce myself:
Sound|1|False|B ong !|nowhitespace
Genie of Success|4|False|I am the Genie of Success, at your service twenty-four hours a day, seven days a week!*

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Genie of Success|1|False|*Excluding weekends and public holidays. Offer may not be combined. Limited to available stock.
Genie of Success|2|False|Instant worldwide marketing campaign!
Genie of Success|3|False|International renown!
Genie of Success|4|False|Stylists and hairdressers included!
Genie of Success|5|False|Immediate results guaranteed!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Genie of Success|1|False|Just one tiny signature at the bottom of this document here and the doorway to glory will open before you!
Pepper|2|False|Wait a minute.
Pepper|4|True|What's up with this Deus ex machina ?!
Pepper|3|False|Right at this very moment, when I'm at my lowest, you fall from the sky with a miracle solution to all my problems?!
Pepper|5|False|It all looks suspicious to me; very suspicious!
Pepper|6|True|Come on Carrot.
Pepper|7|False|This might have worked on us before, but we're no longer foolish enough to fall for this kind of con game!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Bird|1|True|Tweet!
Bird|2|True|Coo!
Bird|3|True|Cooo!
Bird|4|False|Tweet!
Pepper|5|False|You see Carrot, I think that that's what it means to grow up.
Pepper|6|False|Avoid the easy way out; appreciate the value of effort…
Pepper|7|False|…there's no point being jealous of the success of others!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Writing|1|False|Celebrity
Writing|2|False|50 000 Ko
Writing|3|False|Saffron's Success
Writing|4|False|Fashion
Writing|5|False|The Saffron Phenomenon
Pepper|8|False|Fortune will one day smile on us too!
Writing|6|False|Chic
Writing|7|False|Saffron Special
Sound|9|False|Bzeeooo !
Writing|10|False|Celebrity
Writing|11|False|Ms. Pigeon's Fast-paced Life
Writing|12|False|Fashion
Writing|13|False|The Ms. Pigeon Phenomenon
Writing|14|False|Chic
Writing|15|False|Ms. Pigeon Special
Narrator|16|False|- FIN -

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|False|08/2017 - www.peppercarrot.com - Art & Scenario: David Revoy - Translation: Alex Gryson
Credits|2|False|Proofreading and dialog improvements: Alex Gryson, Calimeroteknik, Nicolas Artance, Valvin, and Craig Maloney.
Credits|4|False|Based on the universe of Hereva created by David Revoy with contributions by Craig Maloney. Corrections by Willem Sonke, Moini, Hali, CGand and Alex Gryson.
Credits|5|False|Software: Krita 3.1.4, Inkscape 0.92dev on Linux Mint 18.2 Cinnamon
Credits|6|False|Licence: Creative Commons Attribution 4.0
Credits|8|False|You too can become a patron of Pepper&Carrot at www.patreon.com/davidrevoy
Credits|7|False|Pepper&Carrot is entirely free(libre), open-source and sponsored thanks to the patronage of its readers. For this episode, thanks go to 879 Patrons:
Credits|3|False|Help with storyboarding and mise-en-scène: Calimeroteknik and Craig Maloney.
