# Transcript of Pepper&Carrot Episode 06 [gd]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Tiotal|1|False|Eapasod 6: Co-fharpais nan deoch

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Peabar|1|True|Iochd, thuit mi ’nam chadal gun dùnadh na h-uinneige a-rithist ...
Peabar|2|True|... tha i cho gaothach ...
Peabar|3|False|... agus carson a chì mi Comona air an uinneag?
Peabar|4|False|COMONA!
Peabar|5|False|Co-fharpais nan deoch!
Peabar|6|False|Feumaidh ... gun do thuit mi ’nam chadal gun fhiosda!
Peabar|7|True|... ach ...
Peabar|8|False|Cà’ bheil mi ?!?
Eun|10|False|àg?|nowhitespace
Eun|9|True|mh|nowhitespace

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Peabar|1|False|!!!
Peabar|2|False|A Churrain! Nach tusa an gaisgeach a smaoinich air mo thoirt dhan cho-fharpais!
Peabar|3|False|Mìor-bhail-each !|nowhitespace
Peabar|4|True|Thug thu fiù deoch leinn, mo cuid aodaich agus an ad agam ...
Peabar|5|False|... saoil dè an deoch a thagh thu ...

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Peabar|1|False|GU DÈ ?!!
Àrd-bhàillidh Chomona|3|False|Mar àrd-bhàillidh Chomona, tha mi a’ gairm gu bheil co-fharpais nan deoch ... air fhosgladh !
Àrd-bhàillidh Chomona|4|False|Tha e ’na thoileachas mòr dha ’r baile gu bheil sinn a’ cur fàilte air ceathrar bhana-bhuidseach dhan chiad cho-fharpais againn a-riamh!
Àrd-bhàillidh Chomona|5|True|Cuiribh ur làmhan ri chèile
Àrd-bhàillidh Chomona|6|False|gu mòr
Sgrìobhte|2|False|Co-fharpais deochan Chomona

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
An èisteachd|26|False|Clapan
Àrd-bhàillidh Chomona|1|True|Thàinig i na mìltean mòra o Aonadh nan Teicneolach 's tha e ’na ònar dhomh ur n-aithne a chur air tè thaitneach ’s innleachdach ...
Àrd-bhàillidh Chomona|3|True|Cuiribh ur làmhan ri chèile dh' ar nighean fhèin, bana-bhuidseach Chomona ...
Àrd-bhàillidh Chomona|5|True|Tha an treas co-fharpaisiche againn à dùthaich laighe nan gealach ...
Àrd-bhàillidh Chomona|7|True|... agus mu dheireadh thall, tha an co-fharpaisiche mu dheireadh againn à coille Ceann na Feòraige ...
Àrd-bhàillidh Chomona|2|False|Costag !
Àrd-bhàillidh Chomona|4|False|Cròch !
Àrd-bhàillidh Chomona|6|False|Sidimi !
Àrd-bhàillidh Chomona|8|False|Peabar !
Àrd-bhàillidh Chomona|9|True|Gun tòisicheadh na geamannan!
Àrd-bhàillidh Chomona|10|False|Bhòtaidh sinn le meidh-bualadh-bhasan
Àrd-bhàillidh Chomona|11|False|Tòisicheamaid le taisbeanadh Chostag
Costag|14|False|... na biodh an t-eagal oirbh tuilleadh air a’ bhàs, taing do ...
Costag|15|True|... Dheoch na
Costag|16|False|SÒMBAIDHEACHD !
An èisteachd|17|True|Clapan
An èisteachd|18|True|Clapan
An èisteachd|19|True|Clapan
An èisteachd|20|True|Clapan
An èisteachd|21|True|Clapan
An èisteachd|22|True|Clapan
An èisteachd|23|True|Clapan
An èisteachd|24|True|Clapan
An èisteachd|25|True|Clapan
Costag|13|False|A mhnathan ⁊ a dhaoine-uaisle ...

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Àrd-bhàillidh Chomona|1|False|IONGANTACH !
An èisteachd|3|True|Clapan
An èisteachd|4|True|Clapan
An èisteachd|5|True|Clapan
An èisteachd|6|True|Clapan
An èisteachd|7|True|Clapan
An èisteachd|8|True|Clapan
An èisteachd|9|True|Clapan
An èisteachd|10|True|Clapan
An èisteachd|11|True|Clapan
An èisteachd|12|True|Clapan
An èisteachd|13|True|Clapan
An èisteachd|14|True|Clapan
An èisteachd|15|True|Clapan
An èisteachd|16|False|Clapan
Cròch|18|True|oir seo an deoch
Cròch|17|True|... ach caomhnaibh ur bualadh bhasan, a muinntir Chomona !
Cròch|21|False|... agus am farmad orra!
Cròch|19|False|AGAMSA
Cròch|24|False|TÒISEALACHD !
Cròch|23|True|... Dheoch na
Cròch|22|False|... tha e furasta 's cha leig sibh a leas ach aon bhoinneag bhìodach a ghabhail de ...
An èisteachd|25|True|Clapan
An èisteachd|26|True|Clapan
An èisteachd|27|True|Clapan
An èisteachd|28|True|Clapan
An èisteachd|29|True|Clapan
An èisteachd|30|True|Clapan
An èisteachd|31|True|Clapan
An èisteachd|32|True|Clapan
An èisteachd|33|False|Clapan
Àrd-bhàillidh Chomona|35|False|Feumaidh gun dèan an deoch seo muinntir Chomona beartach!
Àrd-bhàillidh Chomona|34|True|Annasach ! Iongantach !
An èisteachd|36|True|Clapan
An èisteachd|37|True|Clapan
An èisteachd|38|True|Clapan
An èisteachd|39|True|Clapan
An èisteachd|41|True|Clapan
An èisteachd|40|True|Clapan
An èisteachd|43|True|Clapan
An èisteachd|44|True|Clapan
An èisteachd|45|True|Clapan
An èisteachd|46|True|Clapan
An èisteachd|47|True|Clapan
An èisteachd|48|True|Clapan
An èisteachd|49|True|Clapan
An èisteachd|50|True|Clapan
An èisteachd|51|True|Clapan
An èisteachd|52|True|Clapan
Àrd-bhàillidh Chomona|2|False|Dh’fhairtlich Costag am bàs fhèin leis an deoch mhìorbhaileach seo !
Cròch|20|True|’S ann dhan deoch seo a tha sibh uile air bhioran: an tè a chuireas an t-iongnadh air ur nàbaidhean ...
Àrd-bhàillidh Chomona|55|False|Chan urrainn gu bheil ur bualadh bhasan ceàrr. Cha bhuannaich Costag a-nis
An èisteachd|53|True|Clapan
An èisteachd|54|False|Clapan

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Àrd-bhàillidh Chomona|1|False|Bidh e duilich do Shidimi a-nis bàrr a thoirt air an taisbeanadh a bha seo !
Sidimi|4|True|IOCHD!
Sidimi|5|True|Cha bu chòir dhomh, tha e ro chunnartach
Sidimi|7|False|DUILICH !
Àrd-bhàillidh Chomona|3|False|... siuthad a Shidimi, tha a h-uile duine a’ feitheamh ort
Àrd-bhàillidh Chomona|8|False|A mhnathan ⁊ a dhaoine-uaisle ... tha coltas gun dug Sidimi an call roimhpe
Cròch|9|False|Thoir dhomhsa i !
Cròch|10|False|... agus na cuir ort gun robh thu diùid, tha thu a’ milleadh an taisbeanaidh
Cròch|11|False|Tha fios aig a h-uile duine gur mise a bhuannaich a’ cho-fharpais ge b’ e dè nì an deoch agad ...
Sidimi|12|False|!!!
Fuaim|13|False|S S S I I IN|nowhitespace
Sidimi|16|False|UILEBHEIST MHÒIR !
Sidimi|2|False|Cha ... cha robh mi ’n dùil gum biodh againn ris a taisbeanadh
Sidimi|14|True|THOIR AN AIRE!!!
Sidimi|15|True|Seo deoch an
Sidimi|6|True|Tha mi

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Eun|1|False|GRÀG-GRÀG-Grr aa aa aa a a ag|nowhitespace
Fuaim|2|False|BEUM!
Peabar|3|True|... smodaig !
Peabar|5|False|... cuiridh an deoch agam gàire oirbh co-dhiù on a ...
Peabar|4|False|mo chuairt-sa a-nis, an e ?
Àrd-bhàillidh Chomona|6|True|Ruith, òinseach!
Àrd-bhàillidh Chomona|7|False|Tha sinn seachad air a’ cho-fharpais ! ... teich !

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Peabar|2|False|... mar as àbhaist, dh’fhalbh a h-uile duine nuair a thàinig a’ chuairt againne
Peabar|1|True|Sin agad e ...
Peabar|4|True|Co-dhiù no co-dheth, tha beachd agam a-nis air na nì sinn leis an “deoch” agad, a Churrain
Peabar|5|False|... cuiridh sinn rian air cùisean an-seo is dhachaigh leinn an uairsin !
Peabar|7|True|Thusa a
Peabar|8|False|chanèiridh-shòmbaidh-thòiseil-ana-mhòr!
Peabar|10|False|An gabh thu deoch eile ma-thà? ...
Peabar|11|False|... nach gabh, a dhroch-isein ?
Peabar|6|False|HOIGH!
Fuaim|9|False|B R A G !|nowhitespace

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Peabar|1|True|Seadh, leugh an leubail gu cùramach ...
Peabar|2|False|... Dòirtidh mi mu do cheann e gu lèir mura fàg thu Comona làrach nam bonn !
Àrd-bhàillidh Chomona|3|True|Air sgàth ’s gun do shàbhail i ar baile nuair a bha e fo chunnart
Àrd-bhàillidh Chomona|4|False|bheir sinn duais a’ chiad àite do Pheabar airson deoch ... dè bh’ ann ??!!
Peabar|7|False|... an-dà ... a dh’innse na fìrinn, chan e deoch a bh’ ann an da-rìribh ; seo cuid mùin mo chait on turas mu dheireadh dhan lighiche-bheathach!
Peabar|6|True|... Hàhà! seadh ...
Peabar|8|False|... mholainn gun a taisbeanadh ma-thà ...
Neach-aithris|9|False|Eapasod 6: Co-fharpais nan deoch
Neach-aithris|10|False|Deireadh na sgeòil
Sgrìobhte|5|False|50,000 Co
Urram|11|False|Sa Mhàrt 2015 – Obair-ealain is sgeulachd le David Revoy – Eadar-theangachadh le GunChleoc

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Urram|1|False|Tha Pepper&Carrot saor is le tùs fosgailte air fad agus ’ga sponsaireadh le taic nan leughadairean còire. Mòran taing dhan 245 pàtran a thug taic dhan eapasod seo :
Urram|4|False|https://www.patreon.com/davidrevoy
Urram|3|False|’S urrainn dhut dol ’nad phàtran Pepper&Carrot cuideachd airson an ath-eapasod :
Urram|7|False|Innealan : Chaidh 100% dhen eapasod seo a tharraing leis a’ bhathar-bog saor Krita air Linux Mint
Urram|6|False|Bun-tùs fosgailte : tha a h-uile faidhle bun-tùis le breathan is cruthan-clò ri fhaighinn air an làrach-lìn oifigeil
Urram|5|False|Ceadachas : Creative Commons Attribution Faodaidh tu atharrachadh, a cho-roinneadh, a reic is msaa. ...
Urram|2|False|Алексей ★ Глеб Бузало ★ 獨孤欣 & 獨弧悦 ★ Addison Lewis ★ A Distinguished Robot ★ Adrian Lord ★ Ahmad Ali ★ Aina Reich ★ Alandran ★ Alan Hardman ★ Albert Westra ★ Alcide ★ Alejandro Flores Prieto ★ Alex ★ Alexander Bülow Tomassen ★ Alexander Sopicki ★ Alexandra Jordan ★ Alex Lusco ★ Alex Silver ★ Alex Vandiver ★ Alfredo ★ Ali Poulton (Aunty Pol) ★ Allan Zieser ★ Andreas Rieger ★ Andrej Kwadrin ★ Andrew ★ Andrew Godfrey ★ Andrey Alekseenko ★ Angela K ★ Anna Orlova ★ Antan Karmola ★ Anthony Edlin ★ Antonio Mendoza ★ Ardash Crowfoot ★ Arjun Chennu ★ Arne Brix ★ Aslak Kjølås-Sæverud ★ Axel Bordelon ★ Axel Philipsenburg ★ Barbix ★ Ben Evans ★ Bernd ★ Betsy Luntao ★ Boonsak Watanavisit ★ Boris Fauret ★ Boudewijn Rempt ★ BoxBoy ★ Brett Smith ★ Brian Behnke ★ Brian Smith ★ Bryan Butler ★ Bui Dang Hai Trieu ★ Carlos Levischi ★ Charlotte Lacombe-bar ★ Chris Radcliff ★ Chris Sakkas ★ Christian Gruenwaldner ★ Christophe Carré ★ Christopher Bates ★ Clara Dexter ★ codl ★ Colby Driedger ★ Conway Scott Smith ★ Cuthbert Williams ★ Cyrille Largillier ★ Cyril Paciullo ★ Damien ★ Daniel ★ Daniel Björkman ★ Danny Grimm ★ David Tang ★ DiCola Jamn ★ Dmitry ★ Donald Hayward ★ Duke ★ Eitan Goldshtrom ★ Enrico Billich ★ Epsilon ★ Eric Schulz ★ Faolan Grady ★ Francois Schnell ★ Garret Patterson ★ Ginny Hendricks ★ GreenAngel5 ★ Grigory Petrov ★ Guillaume ★ Guillaume Ballue ★ Gustav Strömbom ★ Guy Davis ★ Happy Mimic ★ Helmar Suschka ★ Henning Döscher ★ Ilyas ★ Irina Rempt ★ Ivan Korotkov ★ James Frazier ★ Janusz ★ Jared Tritsch ★ JDB ★ Jean-Baptiste Hebbrecht ★ Jean-Gabriel LOQUET ★ Jeffrey Schneider ★ Jessey Wright ★ Jim ★ Jim Street ★ Jiska ★ Joachim Schiele ★ JoÃ£o Luiz Machado Junior ★ Joern Konopka ★ joe rutledge ★ Johanne Thomson ★ John ★ John Urquhart Ferguson ★ Jónatan Nilsson ★ Jonathan Leroy ★ Jonathan Ringstad ★ Jon Brake ★ Jorge Bernal ★ Joseph Bowman ★ Julien Duroure ★ Justus Kat ★ Kai-Ting (Danil) Ko ★ Kasper Hansen ★ Kate ★ Kathryn Wuerstl ★ Ken Mingyuan Xia ★ Kingsquee ★ Kroet ★ Levi Kornelsen ★ Liselle ★ Lorentz Grip ★ L S ★ Luc Stepniewski ★ Luke Hochrein ★ MacCoy ★ Magnus Kronnäs ★ Manuel ★ Marc & Rick ★ Marcus ★ Martin Owens ★ Mary Brownlee ★ Masked Admirer ★ Mathias Stærk ★ Mefflin Ross Bullis-bates ★ Michael ★ Michael Gill ★ Michael Pureka ★ Michelle Pereira Garcia ★ Mike Mosher ★ Miroslav ★ Muzyka Dmytro ★ Nataya Castillo ★ Nazhif ★ Nicholas DeLateur ★ Nicholas Terranova ★ Nicki Aya ★ Nicola Angel ★ Nicolae Berbece ★ Nicole Heersema ★ Nielas Sinclair ★ NinjaKnight Comics ★ Noble Hays ★ Noelia Calles Marcos ★ Nora Czaykowski ★ Nyx ★ Olivier Amrein ★ Olivier Brun ★ Omar Willey ★ Oscar Moreno ★ Öykü Su Gürler ★ Ozone S. ★ Pablo Lopez Soriano ★ Pat David ★ Patrick Gamblin ★ Paul ★ Pavel Semenov ★ Pet0r ★ Peter ★ Peter Moonen ★ Petr Vlašic ★ Philippe Jean Edward Bateman ★ Pierre Geier ★ Pierre Vuillemin ★ Pranab Shenoy ★ Pyves & Ran ★ Raghavendra Kamath ★ Rajul Gupta ★ Reorx Meng ★ Ret Samys ★ Rictic ★ RJ van der Weide ★ Roberto Zaghis ★ Roman ★ Rumiko Hoshino ★ Rustin Simons ★ Sally Bridgewater ★ Sami T ★ Samuel Mitson ★ Scott Petrovic ★ Sean Adams ★ Shadefalcon ★ ShadowMist ★ Shafak ★ Shawn Meyer ★ Simon Forster ★ Simon Isenberg ★ Sonny W. ★ Soriac ★ Stanislav Vodetskyi ★ Stephanie Cheshire ★ Stephen Bates ★ Stephen Smoogen ★ Steven Bennett ★ Stuart Dickson ★ Surt ★ TamaskanLEM ★ Tar8156 ★ Terry Hancock ★ TheFaico ★ Thomas Citharel ★ Thomas Courbon ★ Thomas Schwery ★ Tim Burbank ★ Tim J. ★ Tomas Hajek ★ Tom Demian ★ Tom Savage ★ Tracey Reuben ★ Travis Humble ★ Tree ★ Tyson Tan ★ Urm ★ Victoria ★ Victoria White ★ Vladislav Kurdyukov ★ Vlad Tomash ★ Westen Curry ★ Witt N. Vest ★ Xavier Claude ★ Yalyn Vinkindo ★ Yaroslav ★ Zeni Pong ★ Źmicier Kušnaroŭ
