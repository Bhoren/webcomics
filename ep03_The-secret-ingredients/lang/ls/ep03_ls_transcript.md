# Transcript of Pepper&Carrot Episode 03 [ls]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Título|1|False|Episodio 3: Ingredientes secretos

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Narrador|1|False|Ciudad Komona; Día de mercado.
Pepper|2|False|Buen día señor, me puede dar ocho estrellas de calabaza, por favor.
Vendedor|3|False|Aquí tienes, serían 60Ko*.
Pepper|5|False|...¡Oh! rayos.
Nota|4|False|* Ko = la moneda de Komona.

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Uhh perdón, pero solamente llevaré cuatro...
Vendedor|2|False|Grrr...
Saffron|3|False|¡Saludos! buen hombre. Por favor, prepare dos docenas de todo para mí. De la mejor calidad, como siempre.
Vendedor|4|False|Siempre es un placer atenderla, Señorita Saffron.
Saffron|5|False|!Hey, pero sí es Pepper!
Saffron|6|False|¡Oh!, déjame adivinar ¿va de maravilla el negocio en el campo?
Pepper|7|False|...
Saffron|8|False|¿Supongo que estarás preparando los ingredientes para el concurso de pociones de mañana?
Pepper|9|True|... ¿un concurso de pociones?
Pepper|10|False|... ¿mañana?

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|4|False|¡Que suerte!, todavía tengo un día para prepararme.
Pepper|5|False|¡Ganemos ese concurso!
Escritura|1|False|Concurso de pociones de Komona
Escritura|2|False|Gran premio 50 000Ko PARA LA MEJOR POCIÓN
Escritura|3|False|Azarday, 3 Pinkmoon Plaza Mayor de Komona

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|2|False|Oh... ¡Lo tengo! ...
Pepper|3|False|...esto es exactamente lo que necesito.
Pepper|4|True|¡Carrot!
Pepper|5|False|Prepárate, me ayudarás a recolectar los ingredientes.
Pepper|1|False|...

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Primero, necesito unas perlas de niebla de estas nubes negras...
Pepper|2|False|...unas bayas rojas de la jungla embrujada.

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|...algunas cáscaras de huevo de los Fénix del valle de los volcanes...
Pepper|2|False|...y finalmente unas gotas de leche de una vaca-dragón joven.

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Eso es Carrot, creo que tengo todo lo que necesito.
Pepper|2|True|Se ve...
Pepper|3|False|...Perfecto.
Pepper|4|True|Mmm...
Pepper|5|True|El mejor... Café... ¡Del mundo!
Pepper|6|False|Es todo lo que necesito para trabajar toda la noche y tener lista la mejor poción para el concurso de mañana.
Narrador|7|False|Continuará...

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Créditos|4|False|Алексей, 獨孤欣, Albert Westra, Alejandro Flores Prieto, Alex Lusco, Alex Silver, Alex Vandiver, Alexander Sopicki, Andreas Rieger, Andrew, Andrew Grady, Andrey Alekseenko, Anna Orlova, Antan Karmola, Antoine, Aslak Kjølås-Sæverud, Axel Bordelon, Axel Philipsenburg, Ben Evans, Boonsak Watanavisit, Boudewijn Rempt, carlos levischi, Charlotte Lacombe-bar, Chris Sakkas, Christophe Carré, Clara Dexter, Colby Driedger, Conway Scott Smith, Dmitry, Eitan Goldshtrom, Enrico Billich,-epsilon-, Garret Patterson, Gustav Strömbom, Guy Davis, Helmar Suschka, Ilyas Akhmedov, Inga Huang, Irene C., Jean-Baptiste Hebbrecht, JEM, Jessey Wright, John, Jónatan Nilsson, Joseph Bowman, Juanjo Fernández Monreal, Jurgo van den Elzen, Kai-Ting (Danil) Ko, Kasper Hansen, Kathryn Wuerstl, Ken Mingyuan Xia, Liselle, Lorentz Grip, MacCoy, Mandy, Martin Owens, Maurice-Marie Stromer, Mauricio Vega, mefflin ross bullis-bates, Michelle Pereira Garcia, Morten Hellesø Johansen, Nazhif, Nicki Aya, Nicolae Berbece, Nicole Heersema, No Reward, Noah Summers, Noble Hays, Olivier Amrein, Olivier Brun, Oscar Moreno, Pavel Semenov, Peter Moonen, Pierre Vuillemin, Ret Samys, Reuben Tracey, Rustin Simons, Sami T, Sean Adams, Shadefalcon,
Créditos|2|False|Este episodio no existiría sin el apoyo de mis 93 mecenas.
Créditos|1|False|Este cómic web es totalmente gratuito y de código abierto (Creative Commons Attribución 3.0 No portada, los archivos en alta resolución están disponibles para descargar).
Créditos|3|False|https://www.patreon.com/davidrevoy
Créditos|6|False|Agradecimientos especiales a: David Tschumperlé (G'MIC) ¡y a todo el equipo de Krita! Traducción : Lewatoto
Créditos|7|False|Este episodio fue realizado con herramientas libres y de código abierto Krita y G'MIC en Xubuntu (GNU/Linux)
Pepper|5|False|¡Gracias!
